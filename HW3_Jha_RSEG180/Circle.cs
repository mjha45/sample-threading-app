﻿using System;
using System.Drawing;
using System.Threading;

namespace HW3_Jha_RSEG180
{
    //Inherit from Shapes class
    class Circle : Shapes
    {
        //A new circle object requires 6 arguments:
        //The first 5 arguments require x coordinate, y coordinate, width, height, color
        //The 6th argument is needed for the Form attributes, in order to match the background color of the circle with the background color of the form
        
        public Circle(int x, int y, int w, int h, Color c, frmShapeMover f)
        {
            //Set the arguments to the value of this class's attributes, which are inherited from the Shape class
            xCoordinate = x; yCoordinate = y; color = c; width = w; height = h; myfrm = f; 
        }  


        public override void paint(Graphics g3) //require the graphics object created on the Form as an argument
        {
                //Render the circle
                g3.DrawEllipse(new System.Drawing.Pen(myfrm.BackColor), xCoordinate, yCoordinate, width, height);

                //Set its coordinates
                xCoordinate = xCoordinate + base.directionX;
                yCoordinate = yCoordinate + base.directionY;

                //Call the ShapeCoordinates function inherited from the Shape class
                base.ShapeCoordinates();
                
                //"Move" the circle to its new position
                g3.DrawEllipse(new System.Drawing.Pen(color), xCoordinate, yCoordinate, width, height);
        }

    }
}
