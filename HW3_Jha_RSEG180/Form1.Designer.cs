﻿namespace HW3_Jha_RSEG180
{
    partial class frmShapeMover
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExit = new System.Windows.Forms.Button();
            this.btnAddShape = new System.Windows.Forms.Button();
            this.btnColor = new System.Windows.Forms.Button();
            this.btnPauseResume = new System.Windows.Forms.Button();
            this.lblShape = new System.Windows.Forms.Label();
            this.lblSpeed = new System.Windows.Forms.Label();
            this.cmbShape = new System.Windows.Forms.ComboBox();
            this.cmbSpeed = new System.Windows.Forms.ComboBox();
            this.pnlCanvas = new System.Windows.Forms.Panel();
            this.lblThreadCount = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(27, 72);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(160, 23);
            this.btnExit.TabIndex = 0;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnAddShape
            // 
            this.btnAddShape.Location = new System.Drawing.Point(27, 12);
            this.btnAddShape.Name = "btnAddShape";
            this.btnAddShape.Size = new System.Drawing.Size(160, 23);
            this.btnAddShape.TabIndex = 1;
            this.btnAddShape.Text = "Add Shape";
            this.btnAddShape.UseVisualStyleBackColor = true;
            this.btnAddShape.Click += new System.EventHandler(this.btnAddShape_Click);
            // 
            // btnColor
            // 
            this.btnColor.Location = new System.Drawing.Point(320, 12);
            this.btnColor.Name = "btnColor";
            this.btnColor.Size = new System.Drawing.Size(77, 23);
            this.btnColor.TabIndex = 2;
            this.btnColor.Text = "Color";
            this.btnColor.UseVisualStyleBackColor = true;
            this.btnColor.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // btnPauseResume
            // 
            this.btnPauseResume.Location = new System.Drawing.Point(27, 43);
            this.btnPauseResume.Name = "btnPauseResume";
            this.btnPauseResume.Size = new System.Drawing.Size(160, 23);
            this.btnPauseResume.TabIndex = 3;
            this.btnPauseResume.Text = "Pause";
            this.btnPauseResume.UseVisualStyleBackColor = true;
            this.btnPauseResume.Click += new System.EventHandler(this.btnPauseResume_Click);
            // 
            // lblShape
            // 
            this.lblShape.AutoSize = true;
            this.lblShape.Location = new System.Drawing.Point(200, 43);
            this.lblShape.Name = "lblShape";
            this.lblShape.Size = new System.Drawing.Size(41, 13);
            this.lblShape.TabIndex = 5;
            this.lblShape.Text = "Shape:";
            // 
            // lblSpeed
            // 
            this.lblSpeed.AutoSize = true;
            this.lblSpeed.Location = new System.Drawing.Point(200, 72);
            this.lblSpeed.Name = "lblSpeed";
            this.lblSpeed.Size = new System.Drawing.Size(41, 13);
            this.lblSpeed.TabIndex = 6;
            this.lblSpeed.Text = "Speed:";
            // 
            // cmbShape
            // 
            this.cmbShape.FormattingEnabled = true;
            this.cmbShape.Items.AddRange(new object[] {
            "Rectangle",
            "Circle"});
            this.cmbShape.Location = new System.Drawing.Point(247, 43);
            this.cmbShape.Name = "cmbShape";
            this.cmbShape.Size = new System.Drawing.Size(150, 21);
            this.cmbShape.TabIndex = 7;
            // 
            // cmbSpeed
            // 
            this.cmbSpeed.FormattingEnabled = true;
            this.cmbSpeed.Items.AddRange(new object[] {
            "5",
            "25",
            "50",
            "75",
            "100"});
            this.cmbSpeed.Location = new System.Drawing.Point(247, 74);
            this.cmbSpeed.Name = "cmbSpeed";
            this.cmbSpeed.Size = new System.Drawing.Size(150, 21);
            this.cmbSpeed.TabIndex = 8;
            // 
            // pnlCanvas
            // 
            this.pnlCanvas.BackColor = System.Drawing.SystemColors.Control;
            this.pnlCanvas.Location = new System.Drawing.Point(12, 130);
            this.pnlCanvas.Name = "pnlCanvas";
            this.pnlCanvas.Size = new System.Drawing.Size(385, 332);
            this.pnlCanvas.TabIndex = 9;
            // 
            // lblThreadCount
            // 
            this.lblThreadCount.AutoSize = true;
            this.lblThreadCount.Location = new System.Drawing.Point(12, 465);
            this.lblThreadCount.Name = "lblThreadCount";
            this.lblThreadCount.Size = new System.Drawing.Size(0, 13);
            this.lblThreadCount.TabIndex = 10;
            // 
            // frmShapeMover
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(409, 486);
            this.Controls.Add(this.lblThreadCount);
            this.Controls.Add(this.pnlCanvas);
            this.Controls.Add(this.cmbSpeed);
            this.Controls.Add(this.cmbShape);
            this.Controls.Add(this.lblSpeed);
            this.Controls.Add(this.lblShape);
            this.Controls.Add(this.btnPauseResume);
            this.Controls.Add(this.btnColor);
            this.Controls.Add(this.btnAddShape);
            this.Controls.Add(this.btnExit);
            this.Name = "frmShapeMover";
            this.Text = "Thread Homework Program";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnAddShape;
        private System.Windows.Forms.Button btnColor;
        private System.Windows.Forms.Button btnPauseResume;
        private System.Windows.Forms.Label lblShape;
        private System.Windows.Forms.Label lblSpeed;
        private System.Windows.Forms.ComboBox cmbShape;
        private System.Windows.Forms.ComboBox cmbSpeed;
        public volatile System.Windows.Forms.Panel pnlCanvas;
        private System.Windows.Forms.Label lblThreadCount;
    }
}

