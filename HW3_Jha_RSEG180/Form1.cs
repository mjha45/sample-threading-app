﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace HW3_Jha_RSEG180
{
    public partial class frmShapeMover : Form
    {

        public bool suspended = false;                      //This lets us toggle the pause/resume function
        public static Color shapeColor = Color.Black;       //This is the shape's color (Black by default)
        public ColorDialog c;                               //This is used for the color selection
        private Hashtable threadHolder = new Hashtable();   //This is the hastable to store all the threads (i.e., each shape)
        private const int shapeSize = 20;                   //This is the shape's size
        public static int threadCount = 0;                  //This keeps count of the number of threads
        public static int speed;                            //This is the sleep duration for a given thread

        public frmShapeMover()
        {
            InitializeComponent();
            cmbShape.SelectedIndex = 0;     //Sets default value of Shape dropdown to Rectangle
            cmbSpeed.SelectedIndex = 0;     //Sets default value of Speed dropdown to 5
        }

        public void createThread(Shapes shape, int speed)   //This is called when a new thread is created
        {
            while (true)                                    //Infinite loop
            {
                Thread.Sleep(speed);                        //Thread will sleep for user-designated time (controls shape's speed)
                lock(this)                                  //Obtain lock on this thread
                {
                    Graphics g = pnlCanvas.CreateGraphics();//Create new graphics object and pass into the paint method
                    shape.paint(g);                         //Call the paint method in whatever class is designated by the 'shape' argument of this method
                    while( suspended )                      
                    {
                        Monitor.Wait(this);                 //Suspend animation by placing thread in WaitSleepJoin state until user toggles back to Resume
                    }
                }

            }
        }


        private void btnAddShape_Click(object sender, EventArgs e)
        {
            Shapes shape;                                   //Create variable of type Shape from my custom class
                                                            //Assign speed value based on user input, which is validated via try/catch
            try
            {
                                                            //The higher the value passed into Sleep(), the slower the shape moves, so subtract it from some value to get the value to pass into Sleep()
                speed = 101 - (Convert.ToInt32(cmbSpeed.Text)); 
            }
            catch(Exception speedex)
            {
                MessageBox.Show(speedex.Message, "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            switch (cmbShape.SelectedIndex)                 //Our shape object will be an instance of a Rectangle or a Circle depending on selected input
            {
                case 0:
                    shape = new Rectangle(0, 0, shapeSize, shapeSize, shapeColor, this);
                    goto createthread;
                case 1:
                    shape = new Circle(0, 0, shapeSize, shapeSize, shapeColor, this);
                    goto createthread;
                default:
                    MessageBox.Show("Invalid shape","Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;                                 //Indicate an error if user doesn't select a proper shape

            }

            createthread:
            Thread t = new Thread(() => createThread(shape, speed));    //This creates a thread points it back to our createThread method using a lamba expression, 
                                                                        //which allows us to pass in the shape and speed we just got from selected input
            threadHolder.Add(threadCount++, t);                         //Add thread to the Hashtable and increment threadCount
            t.Name = "Thread ID: " + threadCount;                       //Name the thread
            t.IsBackground = true;                                      //Sets thread to a background thread b/c we don't need it to keep the program running
            t.Start();                                                  //Start the thread

            lblThreadCount.Text = "Thread Count = " + Convert.ToString(threadCount);    //Label on the bottom of the form will display the number of threads

        }

        private void btnColor_Click(object sender, EventArgs e)
        {
            c = new ColorDialog();                                      //This displays a Color dialog and sets the value of shapeColor attribute
            c.ShowDialog();
            shapeColor = c.Color;
        }

        private void btnPauseResume_Click(object sender, EventArgs e)
        {
            suspended = !suspended;                                     //This toggles the value of suspended when the Pause/Resume button is clicked
            btnPauseResume.Text = btnPauseResume.Text == "Resume" ? "Pause" : "Resume"; //This toggles the button text
            lock (this)
            {
                if (!suspended)                                         //If thread is resumed
                {
                    Monitor.PulseAll(this);                             //Allow ALL threads to continue
                }
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Environment.Exit(Environment.ExitCode);                     //Terminate program and release resources
        }

    }
}
