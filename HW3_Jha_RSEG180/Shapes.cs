﻿using System;
using System.Drawing;
using System.Threading;

namespace HW3_Jha_RSEG180
{
    public abstract class Shapes
    {
        //Shape attributes - these will be required to render the shape
        //These are protected to be accessible by derived class instances (i.e., Rectangle and Circle)
        protected int xCoordinate;
        protected int yCoordinate;
        protected Color color;

        protected int width;
        protected int height;
        protected int directionY = 1;
        protected int directionX = 1;

        //Form is required to set the shape's borders in the ShapeCoordinates method
        protected frmShapeMover myfrm;

        //Constuctor
        public Shapes()
        { }


        public abstract void paint(Graphics g1);    //Abstract modifier used b/c paint method doesn't contain implementation here

        public void ShapeCoordinates()
        {
            //Bounces shape around the borders of the panel
            if ((myfrm.pnlCanvas.Size.Height - 20 < yCoordinate) || (yCoordinate <= 0))
                directionY = directionY * (-1);

            if ((myfrm.pnlCanvas.Size.Width - 20 < xCoordinate) || (xCoordinate <= 0))
                directionX = directionX * (-1);
        }

    }
}
